
import { CREATE_REVIEW, SET_REVIEW } from "../actions/review";

import Review from "../../models/missingpeople";

const initialState = {
  reviews: [],
  userReviews: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_REVIEW:
      return {
        reviews: action.reviews,
        userReviews: action.userReviews,
      };

    case CREATE_REVIEW:
      const newReview = new Review(
        action.reviewData.id,
        action.reviewData.ownerId,
        action.reviewData.review,
        action.reviewData.name
      );
      return {
        ...state,
        reviews: state.reviews.concat(newReview),
      };
  }
  return state;
};


import React, { Component } from "react";
import { View, TextInput, TouchableOpacity, Text } from "react-native";
import { func, bool, string } from "prop-types";
import styles from "./styles";

// the form for loging in and creating account

class BasicFormContainer extends Component {
  state = {
    name: "",
    phonenumber: "",
    adress: "",
    dateofbirth: "",
    email: "",
    password: "",
    nameErrorMessage: "",
    phoneNumberErrorMessage: "",
    addressErrorMessage: "",
    dobErrorMessage: "",
    emailErrorMessage: "",
    passwordErrorMessage: "",
  };

  handleNameChange = (name) => this.setState({ name });
  handlePhonenumberChange = (phonenumber) => this.setState({ phonenumber });

  handleAdressChange = (adress) => this.setState({ adress });
  handleDateofbirthChange = (dateofbirth) => this.setState({ dateofbirth });

  handleEmailChange = (email) => this.setState({ email });

  handlePasswordChange = (password) => this.setState({ password });

  handleButtonPress = () => {
    this.props.onButtonPress({
      name: this.state.name,
      phonenumber: this.state.phonenumber,
      adress: this.state.adress,
      dateofbirth: this.state.dateofbirth,
      email: this.state.email,
      password: this.state.password,
    });
  };

  nameValidation = () => {
    if (this.state.name === "") {
      this.setState({ nameErrorMessage: "Please fill your name" });
    } else {
      this.setState({ nameErrorMessage: "" });
    }
  };
  phoneNumberValidation = () => {
    if (this.state.phonenumber === "") {
      this.setState({
        phoneNumberErrorMessage: "Please fill your phone Number",
      });
    } else {
      this.setState({ phoneNumberErrorMessage: "" });
    }
  };
  addressValidation = () => {
    if (this.state.adress === "") {
      this.setState({ addressErrorMessage: "Please fill your address" });
    } else {
      this.setState({ addressErrorMessage: "" });
    }
  };
  dobValidation = () => {
    if (this.state.dateofbirth === "") {
      this.setState({ dobErrorMessage: "Please fill your date of birth" });
    } else {
      this.setState({ dobErrorMessage: "" });
    }
  };
  emailValidation = () => {
    if (this.state.email === "") {
      this.setState({ emailErrorMessage: "Please fill your email" });
    } else {
      this.setState({ emailErrorMessage: "" });
    }
  };
  passwordValidation = () => {
    if (this.state.password === "") {
      this.setState({ passwordErrorMessage: "Please fill a password" });
    } else {
      this.setState({ passwordErrorMessage: "" });
    }
  };

  render() {
    const {
      name,
      phonenumber,
      adress,
      dateofbirth,
      email,
      password,
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          {this.props.errorMessage ? (
            <Text style={{ color: "red", textAlign: "center" }}>
              {this.props.errorMessage}
            </Text>
          ) : null}

          {this.props.isSignUp && (
            <TextInput
              key="key-0"
              style={styles.textInput}
              placeholder="Name"
              errorText="Please enter a valid Name!"
              returnKeyType="next"
              keyboardType="default"
              autoCapitalize="words"
              onBlur={this.nameValidation}
              onChangeText={this.handleNameChange}
              onEndEditing={this.isvalidd}
              value={name}
              underlineColorAndroid="transparent"
            />
          )}

          <Text style={{ color: "red", textAlign: "center" }}>
            {this.state.nameErrorMessage}
          </Text>

          {this.props.isSignUp && (
            <TextInput
              key="key-1"
              style={styles.textInput}
              placeholder="phonenumber"
              returnKeyType="next"
              errorText="Please enter your phone number!"
              keyboardType="phone-pad"
              autoCapitalize="none"
              onBlur={this.phoneNumberValidation}
              onChangeText={this.handlePhonenumberChange}
              onEndEditing={this.isvalidd}
              value={phonenumber}
              underlineColorAndroid="transparent"
            />
          )}

          <Text style={{ color: "red", textAlign: "center" }}>
            {this.state.phoneNumberErrorMessage}
          </Text>

          {this.props.isSignUp && (
            <TextInput
              key="key-2"
              style={styles.textInput}
              placeholder="adress"
              returnKeyType="next"
              autoCapitalize="none"
              onBlur={this.addressValidation}
              errorText="Please enter your address!"
              keyboardType="default"
              onChangeText={this.handleAdressChange}
              onEndEditing={this.isvalidd}
              value={adress}
              underlineColorAndroid="transparent"
            />
          )}

          <Text style={{ color: "red", textAlign: "center" }}>
            {this.state.addressErrorMessage}
          </Text>

          {this.props.isSignUp && (
            <TextInput
              key="key-3"
              style={styles.textInput}
              placeholder="dateofbirth"
              returnKeyType="next"
              keyboardType="decimal-pad"
              onBlur={this.dobValidation}
              errorText="Please enter a valid dateofbirth!"
              autoCapitalize="none"
              onChangeText={this.handleDateofbirthChange}
              onEndEditing={this.isvalidd}
              value={dateofbirth}
              underlineColorAndroid="transparent"
            />
          )}

          <Text style={{ color: "red", textAlign: "center" }}>
            {this.state.dobErrorMessage}
          </Text>

          <TextInput
            key="key-4"
            style={styles.textInput}
            placeholder="Email"
            returnKeyType="next"
            keyboardType="email-address"
            errorText="Please enter a valid email!"
            autoCapitalize="none"
            onBlur={this.emailValidation}
            onChangeText={this.handleEmailChange}
            onEndEditing={this.isvalidd}
            value={email}
            underlineColorAndroid="transparent"
          />

          <Text style={{ color: "red", textAlign: "center" }}>
            {this.state.emailErrorMessage}
          </Text>

          <TextInput
            key="key-5"
            style={styles.textInput}
            placeholder="Password"
            secureTextEntry
            returnKeyType="done"
            errorText="Please enter the password!"
            onChangeText={this.handlePasswordChange}
            onEndEditing={this.isvalidd}
            onBlur={this.passwordValidation}
            value={password}
            underlineColorAndroid="transparent"
          />

          <Text style={{ color: "red", textAlign: "center" }}>
            {this.state.passwordErrorMessage}
          </Text>

          <TouchableOpacity
            style={styles.button}
            onPress={this.handleButtonPress}
          >
            <Text style={styles.title}>{this.props.title}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

BasicFormContainer.defaultProps = {
  isSignUp: false,
  onSignUpButtonOPress: () => {},
  errorMessage: null,
};

BasicFormContainer.propTypes = {
  onButtonPress: func.isRequired,
  errorMessage: string,
  isSignUp: bool,
  title: string.isRequired,
  onSignUpButtonOPress: func,
};

export default BasicFormContainer;

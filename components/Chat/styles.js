import { StyleSheet } from 'react-native';
//styles for the chat page 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  loading: {
    marginTop: 40
  }
});

export default styles;

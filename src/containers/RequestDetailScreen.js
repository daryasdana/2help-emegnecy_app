/*
this is where I am showing the request details 
*/

import React, { Component,useState } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import MapPreviw from '../../components/MapPreview';

  /* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 

const RequestDetailScreen = props => {
const productId = props.navigation.getParam('productId');
const selectedProduct = useSelector(state =>
    state.Requests.availableRequest.find(prod => prod.id === productId)
  );
  
/* 
  This function will open the map page when the user 
  press on the map and it will show them the
  location where they need that help
*/ 
  const selectedLocation = {lat:selectedProduct.lat, lng:selectedProduct.lng};
  const showMapHandler = () => {
    props.navigation.navigate('Map', {
      readonly: true,
      initialLocation: selectedLocation
    });
  };


  return (
   
    <ScrollView>
      <Image style={styles.image} source={{ uri: selectedProduct.imageUrl }} />
      <View style={styles.actions}>
        
      </View>
      <Text style={styles.price}>£{selectedProduct.price}</Text>
      <Text style={styles.description}>{selectedProduct.description}</Text>
      <Text style={styles.price}>quantityNeeded:{selectedProduct.quantityNeeded}</Text>
      <Text>Location</Text>
      <MapPreviw
      style={styles.mapPreview}
      //  location={{lat:selectedProduct.lat, lng:selectedProduct.lng}}/>
      location={selectedLocation}
          onPress={showMapHandler}
          />

      
    </ScrollView>

  );
};

RequestDetailScreen.navigationOptions = navData => {
  return {
    headerTitle: navData.navigation.getParam('productTitle')
  };
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 300
  },
  actions: {
    marginVertical: 10,
    alignItems: 'center'
  },
  price: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20
  },
  description: {
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: 20
  },
  quantity: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20
  },
  mapPreview: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 300,
    margin: 5
  }
});


export default RequestDetailScreen;

class ProgressUpdate {
    constructor(id, ownerId, title, description, price,imageUrl,name) {
      this.id = id;
      this.ownerId = ownerId;
      this.title = title;
      this.description = description;
      this.price = price;
      this.imageUrl = imageUrl;
      this.name = name;
      
    }
  }
  
  export default ProgressUpdate;
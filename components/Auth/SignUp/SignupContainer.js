import React, { Component } from "react";
import { View, Image } from "react-native";
import { func, bool } from "prop-types";
import { connect } from "react-redux";
import { signupUser, sessionError } from "../../../actions/session/actions";
import CustomActivityIndicator from "../../../components/common/CustomActivityIndicator";
import KeyboardAwareScrollView from "../../../components/common/KeyboardAwareScrollView";
import BasicFormContainer from "../BasicForm/basicForm";
import styles from "../BasicForm/styles";

const LOGO = require("../../../assets/logo.png");
class SignupContainer extends Component {
  componentDidUpdate(prevProps) {
    if (!prevProps.registered && this.props.registered) this.props.onSignedIn();
  }
  componentDidMount() {
    this.props.sessionError("");
  }
  handleSignUp = (params) => this.props.signupUser(params);
  render() {
    return (
      <KeyboardAwareScrollView>
        <View style={styles.imageBox}>
          <Image style={styles.image} source={LOGO} />
        </View>
        <View style={styles.loginBox}>
          {this.props.loading ? (
            <CustomActivityIndicator color="#ffffff" size="large" />
          ) : (
            <BasicFormContainer
              title="Create Account"
              onButtonPress={this.handleSignUp}
              isSignUp
              errorMessage={this.props.errorMessage}
            />
          )}
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

SignupContainer.defaultProps = {
  registered: false,
};

SignupContainer.propTypes = {
  registered: bool,
  onSignedIn: func.isRequired,
  loading: bool.isRequired,
  signupUser: func.isRequired,
};

const mapStateToProps = ({
  sessionReducer: { loading, registered, errorMessage },
}) => ({
  loading,
  registered,
  errorMessage,
});

const mapDispatchToProps = {
  signupUser,
  sessionError,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignupContainer);

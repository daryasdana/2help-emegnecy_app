
import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    Platform,
    ActivityIndicator,
    StyleSheet,
    Alert
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';
import RequestItem from '../../components/shop/RequestItem';
import * as ProgressUpdateActions from '../../store/actions/ProgressUpdate';
import Colors from '../../constants/Colors';

const ProgressUpdate = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const dispatch = useDispatch();
    const userProgressUpdate = useSelector(state => state.ProgressUpdate.userProgressUpdates);
    
 
    const loadProducts = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(ProgressUpdateActions.fetchProgressUpdate());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);

    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadProducts
        );

        return () => {
            willFocusSub.remove();
        };
    }, [loadProducts]);

    useEffect(() => {
        loadProducts();
    }, [dispatch, loadProducts]);

    const selectItemHandler = (id, title) => {
        props.navigation.navigate('ProgressUpdateDetailScreen', {
            productId: id,
            productTitle: title
        });
    };

    const updateItemHandler = (id) => {
        props.navigation.navigate('NewEditProduct', {
            productId: id,
        });
    };

  

    const deleteHandler = (id) => {
        Alert.alert('Are you sure?', 'Do you  want to delete this request?', [
          { text: 'No', style: 'default' },
          {
            text: 'Yes',
            style: 'destructive',
            onPress: () => {
              dispatch(ProgressUpdateActions.deleteProgressUpdate(id));
            }
          }
        ]);
      };

    if (error) {
        return (
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadProducts}
                    color={Colors.primary}
                />
            </View>
        );
    }

    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }

    if (!isLoading && userProgressUpdate.length ===0) {
        return (
            <View style={styles.centered}>
                <Text>You did not upload any progress update!</Text>
            </View>
        );
    }

    return (
        <FlatList
            style={styles.p}
            data={userProgressUpdate}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <RequestItem

                    image={itemData.item.imageUrl}
                    title={itemData.item.title}
                    price={itemData.item.price}
                    onSelect={() => {
                        selectItemHandler(itemData.item.id, itemData.item.title);
                    }}
                >
                    <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title="View Details"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.title);
                        }}
                    />

                     <Button
            color={Colors.primary}
            title="Delete"
            onPress={deleteHandler.bind(this, itemData.item.id)}
          />

                </RequestItem>
            )}
        />
    );
};

ProgressUpdate.navigationOptions = props => {
    const add = () => {
        props.navigation.navigate('AddProgressUpdate');
    };
    return {
        headerTitle: 'Progress Updadate',
        
        headerRight: () => {
           
           
            return <HeaderButtons HeaderButtonComponent={HeaderButton}>
                 
                <Item
                    title="Add"
                    iconName={Platform.OS === 'android' ? 'md-add' : 'ios-add'}
                    onPress={add}
                    
                />
            </HeaderButtons>;
        }

    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    p: {
        // padding: 1,

    },
    pr: {
        padding: 140,
        margin: 100
    },
    btn: {
        marginTop: 100,
        padding: 10,
    }
});

export default ProgressUpdate;

/*
this pahe is where the user can update the personal infromation 
like name,email,phone number, and adress etc
*/
import React, {useEffect, useCallback, useReducer, useState} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Platform,
    KeyboardAvoidingView
} from 'react-native';
import {Button} from 'react-native-paper';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import {useSelector, useDispatch} from 'react-redux';
import HeaderButton from '../../components/UI/HeaderButton';
import * as userInfoActions from '../../store/actions/userInfo';
import Input from '../../components/UI/Input';
import ImgPicker from '../../components/ImagePicher';
import {updateUserInfo} from "../../store/actions/userInfo";

/*
this is to copy all the existing information 
and when the user update the information it will check if what they inputted is valid
*/

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        const updatedValidates = {
            ...state.inputValidates,
            [action.input]: action.isValid
           
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidates) {
            updatedFormIsValid = updatedFormIsValid && updatedValidates[key];

        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidates: updatedValidates,
            inputValues: updatedValues
        };
    }
    return state;
};

const EditProfilePage = props => {

    const prodId = props.navigation.getParam('userInfoId');
    const editedUserInfro = useSelector(state =>
        state.userInfo.userUserInfo.find(prod => prod.id === prodId)
      
    );


    const [selectedImage, setSelectedImage] = useState(prodId ? editedUserInfro.imageUrl : '');
    const dispatch = useDispatch();

    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            namee: editedUserInfro.namee,
            email: editedUserInfro.email,
            dateofbirth: editedUserInfro.dateofbirth,
            phonenumber: editedUserInfro.phonenumber,
            adress: editedUserInfro.adress,
        },
        inputValidates: {
            namee: true,
            email:true,
            dateofbirth:true,
            phonenumber: true,
            adress:true,
        },
        formIsValid: true
    });

    const imageTakenHandler = imageUri => {
        setSelectedImage(imageUri);
    };

       /*
    this is where I am sending the user's input to the store action and it will
    update them on the data base is the form is valid. 
    if it is not valid it will retun to the form.
    */

    const submitHandler = useCallback(() => {
     
        formState.inputValues.imageUrl = selectedImage;
        dispatch(
            userInfoActions.updateUserInfo(
                prodId,
                formState.inputValues.namee,
                formState.inputValues.email,
                formState.inputValues.dateofbirth,
                formState.inputValues.phonenumber,
                formState.inputValues.adress,
                formState.inputValues.imageUrl,

            )
        );
        props.navigation.goBack();
    }, [dispatch, prodId, formState]);

    useEffect(() => {
        props.navigation.setParams({submit: submitHandler});
    }, [submitHandler]);

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            });
        },
        [dispatchFormState]
    );

    return (
        <KeyboardAvoidingView
            style={{flex: 1}}
            behavior="padding"
            keyboardVerticalOffset={100}
        >
            <ScrollView>
                <View style={styles.form}>
                    <Input
                        
                        id="namee"
                        label="Full Name"
                        errorText="Please enter a valid name!"
                        keyboardType="default"
                        autoCapitalize="sentences"
                        autoCorrect
                        returnKeyType="next"
                        initialValue={editedUserInfro.namee}
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                       id="email"
                       label="email"
                       errorText="Please enter a valid email"
                       keyboardType="default"
                       autoCapitalize="sentences"
                       returnKeyType="next"
                       initialValue={editedUserInfro.email}
                       onInputChange={inputChangeHandler}
                    />

                    <Input
                         id="dateofbirth"
                         label="date of birth"
                         errorText="Please enter a valid date of birth!"
                         keyboardType="numeric"
                         returnKeyType="next"
                         initialValue={editedUserInfro.dateofbirth}
                         onInputChange={inputChangeHandler}
                    />

<Input
                        id="phonenumber"
                        label="phone number"
                        errorText="Please enter the phone number!"
                        keyboardType="numeric"
                        initialValue={editedUserInfro.phonenumber}
                        onInputChange={inputChangeHandler}
                    />
                        <Input
                        id="adress"
                        label="address"
                        errorText="Please enter the address!"
                        keyboardType="default"
                        initialValue={editedUserInfro.adress}
                        onInputChange={inputChangeHandler}
                    />

                    <ImgPicker onImageTaken={imageTakenHandler}/>

                    <Button onPress={submitHandler}>Update</Button>

                </View>


                

            </ScrollView>

        </KeyboardAvoidingView>

    );
};

// const style = StyleSheet.create({
//     image: {
//         width: '100%',
//         height: 250,
//     }
// });


EditProfilePage.navigationOptions = navData => {
    const submitFn = navData.navigation.getParam('submit');
    return {
        headerTitle: 'Edit Profile',
        headerRight: () => {
            return <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Add"
                    iconName={Platform.OS === 'android' ? 'md-checkmark' : 'ios-checkmark'}
                    onPress={submitFn}
                />
            </HeaderButtons>;
        }


    };
};

const styles = StyleSheet.create({
    form: {
        margin: 20,

    }
});

export default EditProfilePage;

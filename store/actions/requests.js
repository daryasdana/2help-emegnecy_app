//this page is responsible for adding the form the the database or update or delete them 
import Product from '../../models/product';

export const DELETE_REQUEST = 'DELETE_REQUEST';
export const CREATE_REQUEST = 'CREATE_REQUEST';
export const UPDATE_REQUEST = 'UPDATE_REQUEST';
export const SET_REQUEST = 'SET_REQUEST';
import ENV from '../../env';
import * as firebase from "firebase";

export const fetchRequest = () => {
    return async dispatch => {
        // any async code you want!
        const userId = firebase.auth().currentUser.uid;
        try {
            const response = await fetch(
                'https://help-6faf1.firebaseio.com/requests.json'
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }

            const resData = await response.json();
            const Requests = [];

            for (const key in resData) {
                Requests.push(
                    new Product(
                        key,
                        resData[key].ownerId,
                        resData[key].title,
                        resData[key].description,
                        resData[key].price,
                        resData[key].quantityNeeded,
                        resData[key].imageUrl,
                        resData[key].lat,
                        resData[key].lng,
                    )
                );
            }

            dispatch({
                type: SET_REQUEST,
                Requests: Requests,
                userRequests:Requests.filter(prod => prod.ownerId === userId)
            });
            

        } catch (err) {
  
            throw err;
        }
    };
};


export const deleteRequest = productId => {
    return async dispatch => {
        await fetch(
            `https://help-6faf1.firebaseio.com/requests/${productId}.json`,
            {
                method: 'DELETE',
                
                
            }
        );
        dispatch({type: DELETE_REQUEST, pid: productId});
    };
};


const getImageFromFirebase = async (uri) => {
    const imageName = Math.round(+new Date()/1000) + "." + uri.substr(uri.lastIndexOf('.') +1);
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase.storage().ref().child("images/" + imageName);
    const snapshot = await ref.put(blob);
    const imgURL = await snapshot.ref.getDownloadURL();
    return imgURL
};



export const createRequests = (title, description, price, quantityNeeded, imageUri,location) => {
    return async dispatch => {
        const userId = firebase.auth().currentUser.uid;
        const imageUrl = await getImageFromFirebase(imageUri);
        const lat = location.lat;
        const lng = location.lng;
        const payload = JSON.stringify({
            title,
            description,
            price,
            ownerId: userId,
            quantityNeeded,
            imageUrl,
            lat,
            lng,
            
        });
    
        const response = await fetch(
            'https://help-6faf1.firebaseio.com/requests.json',
            

            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: payload,
               

            }
        );

        const resData = await response.json();
        
        dispatch({
            type: CREATE_REQUEST,
            requestData: {
                id: resData.name,
                title,
                description,
                price,
                ownerId: userId,
                quantityNeeded,
                imageUrl,
                lat,
                lng

            }
        });
    };
};



export const updateRequests = (id, title, description,quantityNeeded, imageUri) => {
    return async dispatch => {

        const imageUrl = await getImageFromFirebase(imageUri);
        await fetch(
            `https://help-6faf1.firebaseio.com/requests/${id}.json`,
            {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title,
                    description,
                    quantityNeeded,
                    imageUrl,
                })
            }
        );

        dispatch({
            type: UPDATE_REQUEST,
            pid: id,
            requestData: {
                title,
                description,
                quantityNeeded,
                imageUrl,
            }
        });
    };
};

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native';

import { Ionicons } from '@expo/vector-icons';

export default class Home extends React.Component {

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View>
                <View style={styles.container}>
                    <Image
                        style={styles.image}
                        source={require('../../assets/logo.png')}/>
                </View>
                <Text style={styles.Welcome}>Emergency Teams</Text>
                <View>
                <TouchableOpacity
                        style={styles.userBtn}
                        onPress={() => navigate('AddRequests')}>
                        <View style={styles.test}>

                            <Ionicons name="ios-add-circle-outline" style={styles.Icon}/>
                            <Text style={styles.btnTxt}> Add Requests </Text>
                        </View>
                    </TouchableOpacity>
                <TouchableOpacity
                        style={styles.userBtn}
                        onPress={() => navigate('UserRequestsScreen')}>
                        <View style={styles.test}>

                            <Ionicons name="ios-folder-open" style={styles.Icon}/>
                            <Text style={styles.btnTxt}> Uploaed Requests </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.userBtn}
                        onPress={() => navigate('NewRequests')}>
                        <View style={styles.test}>
                            <Ionicons name="ios-list" style={styles.Icon}/>
                            <Text style={styles.btnTxtt}>All Requests</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.userBtn}
                        onPress={() => navigate('AcceptedRequests'
                        )}>
                        <View style={styles.test}>

                            <Ionicons name="ios-checkmark-circle-outline" style={styles.Icon}/>
                            <Text style={styles.btnTxt}> Accepted Requests</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.userBtn}
                        onPress={() => navigate('ProgressUpdate'
                        )}>
                        <View style={styles.test}>

                            <Ionicons name="ios-trending-up" style={styles.Icon}/>
                            <Text style={styles.btnTxt}> Progress Update</Text>
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        style={styles.userBtn}
                        onPress={() => navigate('AllMissingPeoples'
                        )}>
                        <View style={styles.test}>

                            <Ionicons name="ios-contact" style={styles.Icon}/>
                            <Text style={styles.btnTxt}> Missing Peoples</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        marginTop: -20,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },

    image: {
        marginBottom: -200,
        width: 130,
        height: 130,
        resizeMode: 'contain',
    },

    Icon: {
        fontSize: 30,
        color: '#fff',
        padding: 5,
        margin: 2,
    },

    test: {
        flexDirection: 'row',
        justifyContent: "space-between",
        width: "73%",
    },

    btnTxt: {
        color: '#fff',
        fontSize: 20,
        //textAlign: "right",
        padding: 15,
        textAlign: 'center',
    },
    btnTxtt: {
        color: '#fff',
        fontSize: 20,
        //textAlign: "left",
        padding: 15,
        marginHorizontal:20
        //textAlign: 'center',
    },

    Welcome: {
        textAlign: 'center',
        marginTop: 150,
        marginBottom: 40,
        fontSize: 20,
        color: '#B00020'

    },

    userBtn: {
        backgroundColor: "#F06292",
        padding: 2,
        borderRadius: 8,
        margin: 10,
    },


});










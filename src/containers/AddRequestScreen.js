
import React, {useEffect, useCallback, useReducer, useState} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
  
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import {Button} from 'react-native-paper';
import { useDispatch} from 'react-redux';
import * as productsActions from '../../store/actions/requests';
import Input from '../../components/UI/Input';
import ImgPicker from '../../components/ImagePicher';
import LocationPicker from '../../components/LocationPicker';


const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        const updatedValidates = {
            ...state.inputValidates,
            [action.input]: action.isValid
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidates) {
            updatedFormIsValid = updatedFormIsValid && updatedValidates[key];
        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidates: updatedValidates,
            inputValues: updatedValues
        };
    }
    return state;
};

const AddRequestScreen = props => {

//this useState is to capture what the user input
    const [selectedImage, setSelectedImage] = useState('');
    const [selectedLocation, setSelectedLocation] = useState('');

    //const prodId = props.navigation.getParam('productId');
    // const editedProduct = useSelector(state =>
    //     state.Requests.userRequests.find(prod => prod.id === prodId)
    // );

    const dispatch = useDispatch();
/*
this is where I am doing the user's input validation
if the fields are empty the form is not valid and this will show them a error
*/

    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            title: '',
            imageUrl: '',
            description: '',
            price: '',
            quantityNeeded: '',
        },
        inputValidates: {
            title: false,
            description: false,
            price: false,
            quantityNeeded: false,
        },
        formIsValid: false
    });

    let imageTakenHandler;
    imageTakenHandler = imagePath => {
        setSelectedImage(imagePath);
    };
    let locationPickedHandler = useCallback(location => {
        console.log(location);
        setSelectedLocation(location);
    }, []);


    let validForm = () => {
        if (!formState.formIsValid) {
            Alert.alert('Wrong input!', 'Please check the errors in the form.', [
                {text: 'OK'}
            ]);
            return false
        }
        return true
    };
    /*
    this is where I am sending the user's input to the store action and it will
    save them on the data base is the form is valid. 
    if it is not valid it will retun to the form.
    */
    const addRequestHandler = useCallback(() => {
        if (!validForm()) {
            return
        }
        dispatch(
            productsActions.createRequests(
                formState.inputValues.title,
                formState.inputValues.description,
                +formState.inputValues.price,
                +formState.inputValues.quantityNeeded,
                selectedImage,
                selectedLocation
            )
        );
        alert('Rrequest Submitted Successfully ')
 //if the form is valid and saved in the data back it will return to the home page

       //after the request is submitted retun back to the UserRequestsScreen
       props.navigation.goBack();
        props.navigation.navigate('UserRequestsScreen');
    });

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            });
        },
        [dispatchFormState]
    );
/*
this is where the user fill the form
I am using the KeyboardAvoidingView do that the user can alway see what they are typing
*/
    return (
        <KeyboardAvoidingView
            style={{flex: 1}}
            behavior="padding"
            keyboardVerticalOffset={100}
        >
            <ScrollView>
                <View style={styles.form}>
                    <Input
                        id="title"
                        label="Title"
                        errorText="Please enter a valid title!"
                        keyboardType="default"
                        autoCapitalize="sentences"
                        autoCorrect
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="price"
                        label="Price"
                        errorText="Please enter a valid price!"
                        keyboardType="decimal-pad"
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="description"
                        label="Description"
                        errorText="Please enter a valid description!"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCapitalize="sentences"
                        autoCorrect
                        multiline
                        numberOfLines={3}
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="quantityNeeded"
                        label="Quantity"
                        errorText="Please enter the quantity needed!"
                        keyboardType="decimal-pad"
                        onInputChange={inputChangeHandler}
                    />

                    <ImgPicker onImageTaken={imageTakenHandler}/>
                    <LocationPicker navigation={props.navigation} onLocationPicked={locationPickedHandler}/>
                    <Button onPress={addRequestHandler}>Add Request</Button>

                </View>

            </ScrollView>

        </KeyboardAvoidingView>

    );


};




const styles = StyleSheet.create({
    form: {
        margin: 20,
    }
});

export default AddRequestScreen;

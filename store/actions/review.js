
import Review from "../../models/review";
export const CREATE_REVIEW = "CREATE_REVIEW";
export const SET_REVIEW = "SET_REVIEW";
import * as firebase from "firebase";

export const fetchReview = (userInfoId) => {
  return async (dispatch) => {
    const userId = firebase.auth().currentUser.uid;
    const name = firebase.auth().currentUser.displayName

    try {
      const response = await fetch(
        "https://help-6faf1.firebaseio.com/Review.json"
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const resData = await response.json();
      const reviews = [];

      for (const key in resData) {
        reviews.push(
          new Review(
            key, 
            resData[key].ownerId,
            resData[key].review,
            resData[key].name
             
             )
        );
      }

      dispatch({
        type: SET_REVIEW,
        reviews: reviews,
        userReviews: reviews.filter((prod) => prod.ownerId === userInfoId),
      });
      //console.log(Requests)
    } catch (err) {
      // send to custom analytics server
      throw err;
    }
  };
};

export const createReview = (review, userInfoId) => {
  return async (dispatch) => {
    const name = firebase.auth().currentUser.displayName


    const payload = JSON.stringify({
      review,
      ownerId: userInfoId,
      name
    });

    // any async code you want!
    const response = await fetch(
      "https://help-6faf1.firebaseio.com/Review.json",

      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: payload,
      }
    );

    const resData = await response.json();
    //console.log(resData);

    dispatch({
      type: CREATE_REVIEW,
      reviewData: {
        id: resData.name,
        review,
        ownerId: userInfoId,
        name
      },
    });
  };
};

class Product {
    constructor(id, ownerId, title, description, price,quantityNeeded,imageUrl,lat,lng) {
      this.id = id;
      this.ownerId = ownerId;
      this.title = title;
      this.description = description;
      this.price = price;
      this.quantityNeeded = quantityNeeded;
      this.imageUrl = imageUrl;
      this.lat = lat;
      this.lng = lng;
    }
  }
  
  export default Product;

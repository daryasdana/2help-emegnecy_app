
import React, { useEffect, useState, useCallback } from "react";
import {
  View,
  FlatList,
  Text,
  ActivityIndicator,
  StyleSheet,

} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import AcceptedItem from "../../components/shop/AcceptedItem";
import * as ordersActions from "../../store/actions/acceptedUsers";
import Colors from "../../constants/Colors";
import * as productsActions from "../../store/actions/requests";

const AcceptedRequests = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const acceptedVW = useSelector((state) => state.acceptedVW.acceptedVW);
  const dispatch = useDispatch();
  const Requests = useSelector((state) => state.Requests.availableRequest);
  const [error, setError] = useState();

  /* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 

  const loadAcceptedRequests = useCallback(async () => {
    setError(null);
    setIsLoading(true);
    try {
      await dispatch(productsActions.fetchRequest());
      await dispatch(ordersActions.fetchOrders());
    } catch (err) {
      setError(err.message);
    }
    setIsLoading(false);
  }, [dispatch, setIsLoading, setError]);
  useEffect(() => {
    loadAcceptedRequests();
  }, [dispatch, loadAcceptedRequests]);


   /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */ 

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }
  //If No one accepted any job it will show this message 
  if (!isLoading && acceptedVW.length === 0) {
    return (
      <View style={styles.centered}>
        <Text>No one accepted your request yet!</Text>
      </View>
    );
  }

  /*
    this is for rendering the data and extracting them by their unique id
    I am using a FlatList because I don't know how long the list might be
    and flatlist is better at rendering longer list
*/
  return (
    <FlatList
      data={Requests}
      keyExtractor={(item) => item.id}
      renderItem={(itemData) => (
        <AcceptedItem
          props={props}
          navigation={props.navigation}
          productId={itemData.item.id}
          items={itemData.item.items}
          title={itemData.item.title}
          price={itemData.item.price}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default AcceptedRequests;


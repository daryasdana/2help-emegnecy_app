import React, { Component,useState } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { useSelector } from 'react-redux';

const ProgressUpdateDetailScreen = props => {
const ProgressUpdateId = props.navigation.getParam('productId');
const selectedProduct = useSelector(state =>
    state.ProgressUpdate.availableProgressUpdates.find(prod => prod.id === ProgressUpdateId)
  );

  return (
   
    <ScrollView>
      <Image style={styles.image} source={{ uri: selectedProduct.imageUrl }} />
      <View style={styles.actions}>
      </View>
      <Text style={styles.name}>By: {selectedProduct.name}</Text>
      <Text style={styles.price}>£{selectedProduct.price}</Text>
      <Text style={styles.description}>{selectedProduct.description}</Text>
      
    </ScrollView>

  );
};

ProgressUpdateDetailScreen.navigationOptions = navData => {
  return {
    headerTitle: navData.navigation.getParam('productTitle')
  };
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 300
  },
  actions: {
    marginVertical: 10,
    alignItems: 'center'
  },
  price: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20
  },
  description: {
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: 20
  },
  name: {
    fontSize: 20,
    textAlign: 'center',
    marginHorizontal: 20
  },
  quantity: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20
  },
  mapPreview: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 300,
    margin: 5
  }
});


export default ProgressUpdateDetailScreen;

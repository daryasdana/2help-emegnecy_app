//this page is responsible for adding the form the the database or update or delete them 
import UserInfo from '../../models/userInfo';


export const ADD_USERINFO = 'ADD_USERINFO';
export const UPDATE_USERINFO = 'UPDATE_USERINFO';
export const SET_USERINFO = 'SET_USERINFO';

import * as firebase from "firebase";

export const fetchUserInfo = () => {
    return async dispatch => {
        // any async code you want!
        const userId = firebase.auth().currentUser.uid;
        try {
            const response = await fetch(
                `https://help-6faf1.firebaseio.com/Users.json`
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }

            const resData = await response.json();
            const userInfo = [];
            

            for (const key in resData) {
              userInfo.push(
                    new UserInfo(
                        key,
                        resData[key].id,
                        resData[key].name,
                        resData[key].email,
                        resData[key].dateofbirth,
                        resData[key].phonenumber,
                        resData[key].adress,
                        resData[key].avatar,
                        resData[key].ownerId,
                        
                    )
                );
            }
            //console.log(resData);
           

            dispatch({
                type: SET_USERINFO,
                userInfo: userInfo,
                userUserInfoo: userInfo,
                userUserInfo: userInfo.filter(prod => prod.id === userId),
               
                
            });
           
        } catch (err) {
            
            throw err;
        }
    };
};


const getImageFromFirebase = async (uri) => {
    const imageName = Math.round(+new Date()/1000) + "." + uri.substr(uri.lastIndexOf('.') +1);
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase.storage().ref().child("userImages/" + imageName);
    const snapshot = await ref.put(blob);
    const imgURL = await snapshot.ref.getDownloadURL();
    return imgURL
};



export const addUserInfro = (name, email, dateofbirth, phonenumber,adress,imageUri) => {
    return async dispatch => {

        const userId = firebase.auth().currentUser.uid;
        const avatar = await getImageFromFirebase(imageUri);

        const payload = JSON.stringify({
            name,
            email,
            id: userId,
            dateofbirth,
            phonenumber,
            adress,
            avatar,
        });

        // any async code you want!
        const response = await fetch(
            `https://help-6faf1.firebaseio.com/Users.json`,
            

            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: payload,
               

            }
        );

        const resData = await response.json();
    
        dispatch({
            type: ADD_USERINFO,
            userInfoData: {
              id:resData.name,
                name,
                email,
                id: userId,
                dateofbirth,
                phonenumber,
                adress,
                avatar,
                
            }
        });
    };
};



export const updateUserInfo = (id, name, email,dateofbirth, phonenumber,adress,imageUri) => {
    return async dispatch => {
        const avatar = await getImageFromFirebase(imageUri);       
        await fetch(
            `https://help-6faf1.firebaseio.com/Users/${id}.json`,
            {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify({
                    name,
                    email,
                    dateofbirth,
                    phonenumber,
                    adress,
                    avatar,
               
                })
            }
        );


        dispatch({
            type: UPDATE_USERINFO,
            pid: id,
            userInfoData: {
                name,
                email,
                dateofbirth,
                phonenumber,
                adress,
                avatar,
            }
        });
    };
};






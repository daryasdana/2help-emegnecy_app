import React, { Component } from "react";
import { View, Image, Text } from "react-native";
import { func, bool } from "prop-types";
import { connect } from "react-redux";
import {
  loginUser,
  restoreSession,
  sessionError,
} from "../../../actions/session/actions";
import CustomActivityIndicator from "../../../components/common/CustomActivityIndicator";
import KeyboardAwareScrollView from "../../../components/common/KeyboardAwareScrollView";
import BasicFormComponent from "../BasicForm/basicForm";
import styles from "../BasicForm/styles";

const LOGO = require("../../../assets/logo.png");
class SingInContainer extends Component {
  componentDidMount() {
    this.props.restoreSession();
    this.props.sessionError("");
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.logged && this.props.logged) {
      this.props.onSignedIn();
    }
  }
  handleSignIn = (params) => this.props.loginUser(params);
  render() {
    return (
      <KeyboardAwareScrollView>
        <View style={styles.imageBox}>
          <Image style={styles.image} source={LOGO} />
          <Text style={styles.nameOfApp}>Log in to 2Help</Text>
        </View>
        <View style={styles.loginBox}>
          {this.props.loading ? (
            <CustomActivityIndicator color="#ffffff" size="large" />
          ) : (
            <React.Fragment>
              <BasicFormComponent
                title="Sign In"
                onButtonPress={this.handleSignIn}
                onSignUpButtonOPress={this.props.onSignUp}
                errorMessage={this.props.errorMessage}
              />
            </React.Fragment>
          )}
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

SingInContainer.propTypes = {
  restoreSession: func.isRequired,
  logged: bool.isRequired,
  loading: bool.isRequired,
  onSignedIn: func.isRequired,
  onSignUp: func.isRequired,
  loginUser: func.isRequired,
};

const mapStateToProps = ({
  sessionReducer: { loading, user, errorMessage, logged },
}) => ({
  loading,
  user,
  errorMessage,
  logged,
});

const mapDispatchToProps = {
  loginUser,
  restoreSession,
  sessionError,
};

export default connect(mapStateToProps, mapDispatchToProps)(SingInContainer);

import React, {useState} from 'react';
import {View, Button, Image, Text, StyleSheet, Alert} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

import Colors from '../constants/Colors';

const ImgPicker = props => {
const [pickedImage, setPickedImage] = useState('');
 const verifyPermissions = async () => {
        // in here we are asking for camera permition 
        const result = await Permissions.askAsync(
            
            Permissions.CAMERA
            );

            /*
            If the user did not give permission 
            alert the user that they need permission for opening the camera
            */

        if (result.status !== 'granted') {
            Alert.alert(
                'Insufficient permissions!',
                'You need to grant camera permissions to use this app.',
                [{text: 'OK'}]
            );
            return false;
        }
        return true;

    };

/*
This is the function that will open the camera
if the user gave permetion to open it.if not it will 
return and not open the camera 
*/
    const takeImageHandler = async () => {
        const hasPermission = await verifyPermissions();
        if (!hasPermission) {
            return;
        }
        const image = await ImagePicker.launchCameraAsync({
            //allowsEditing:true,
            aspect: [16, 9],
            quality: 0
        });
        if (!image.cancelled) {
            setPickedImage(image.uri);
            props.onImageTaken(image.uri);
            
        }

    };
    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1
        });

        if (!result.cancelled) {
            setPickedImage(result.uri);
            props.onImageTaken(result.uri);
            
        }
    }
    return (
        <View style={styles.imagePicker}>
            <View style={styles.imagePreview}>
                {!pickedImage ? <Text>No image picked yet.</Text>
                    : <Image style={styles.image} source={{uri: pickedImage}}/>}
            </View>
            <Button
                title="Take Image"
                color={Colors.primary}
                onPress={takeImageHandler}
            />
            <Button
          title="Pick an image from camera roll"
          onPress={this._pickImage}
             />
        </View>
    );
};

const styles = StyleSheet.create({
    imagePicker: {
        alignItems: 'center',
        marginBottom: 15

    },
    imagePreview: {
        width: '100%',
        height: 250,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#ccc',
        borderWidth: 1
    },
    image: {
        width: '100%',
        height: '100%'
    }
});

export default ImgPicker;

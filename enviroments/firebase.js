/* eslint-disable import/no-extraneous-dependencies */
import firebase from '@firebase/app';
import '@firebase/auth';
import '@firebase/database';

const config = {
  apiKey: "AIzaSyA2BTyDJH5I0mfHYkwlnCQz7NhVJVIuBBs",
    authDomain: "help-6faf1.firebaseapp.com",
    databaseURL: "https://help-6faf1.firebaseio.com",
    projectId: "help-6faf1",
    storageBucket: "help-6faf1.appspot.com",
    messagingSenderId: "756170280314",
    appId: "1:756170280314:web:9a327b6dfb097ad0919ac3",
    measurementId: "G-J67MZBQQGS"
};

let instance = null;

class FirebaseService {
  constructor() {
    if (!instance) {
      this.app = firebase.initializeApp(config);
      instance = this;
    }
    return instance;
  }
}

const firebaseService = new FirebaseService().app;
export default firebaseService;

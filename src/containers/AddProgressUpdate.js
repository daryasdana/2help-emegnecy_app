
import React, {useEffect, useCallback, useReducer, useState} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import {Button} from 'react-native-paper';
import {useDispatch} from 'react-redux';
import * as ProgressUpdateActions from '../../store/actions/ProgressUpdate';
import Input from '../../components/UI/Input';
import ImgPicker from '../../components/ImagePicher';


const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        const updatedValidates = {
            ...state.inputValidates,
            [action.input]: action.isValid
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidates) {
            updatedFormIsValid = updatedFormIsValid && updatedValidates[key];
        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidates: updatedValidates,
            inputValues: updatedValues
        };
    }
    return state;
};

const AddProgressUpdate = props => {

    const [selectedImage, setSelectedImage] = useState('');
    const dispatch = useDispatch();
    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            title: '',
            imageUrl: '',
            description: '',
            price: '',
           
        },
        inputValidates: {
            title: false,
            description: false,
            price: false,
           
        },
        formIsValid: false
    });

    let imageTakenHandler;
    imageTakenHandler = imagePath => {
        setSelectedImage(imagePath);
    };

  
    let validForm = () => {
        if (!formState.formIsValid) {
            Alert.alert('Wrong input!', 'Please check the errors in the form.', [
                {text: 'OK'}
            ]);
            return false
        }
        return true
    };

    const addProgressUpdate = useCallback(() => {
        if (!validForm()) {
            return
        }
        dispatch(
            ProgressUpdateActions.addProgressUpdate(
                formState.inputValues.title,
                formState.inputValues.description,
                formState.inputValues.price,
                selectedImage,
                
            )
        );
        props.navigation.goBack();
        //console.log(selectedLocation);
    });

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            });
        },
        [dispatchFormState]
    );

    return (
        <KeyboardAvoidingView
            style={{flex: 1}}
            behavior="padding"
            keyboardVerticalOffset={100}
        >
            <ScrollView>
                <View style={styles.form}>
                    <Input
                        id="title"
                        label="Title"
                        errorText="Please enter a valid title!"
                        keyboardType="default"
                        autoCapitalize="sentences"
                        autoCorrect
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="price"
                        label="price"
                        errorText="Please enter Total Donation Received!"
                        keyboardType="decimal-pad"
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="description"
                        label="Description"
                        errorText="Please enter a valid description!"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCapitalize="sentences"
                        autoCorrect
                        multiline
                        numberOfLines={3}
                        onInputChange={inputChangeHandler}
                    />

                    <ImgPicker onImageTaken={imageTakenHandler}/>
                   
                    <Button onPress={addProgressUpdate}>Add Request</Button>

                </View>


                   

            </ScrollView>

        </KeyboardAvoidingView>

    );


};




const styles = StyleSheet.create({
    form: {
        margin: 20,
    }
});

export default AddProgressUpdate;

import { StyleSheet } from 'react-native';

const button = {
  alignItems: 'center',
  justifyContent: 'center',
  height: 40,
  marginHorizontal: 10,
  marginVertical: 5,
  borderRadius: 5,
  padding: 3
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    maxWidth: 300,
    minWidth: 300
  },
  textInput: {
    backgroundColor: '#ffffff',
    padding: 10,
    height: 40,
    margin: 10,
    borderRadius: 5
  },
  button: {
    ...button,
   backgroundColor: '#F5BD10'
  },
  signUpButton: {
    ...button
  },
  title: {
    color: '#070265',
    fontSize: 18,
    fontWeight: 'bold'
  },
  loginBox: {
    margin: 10
  },
  imageBox: {
    alignItems: 'center',
    marginTop: 20
  },
  image: {
    width: 150,
    height: 150
  },
  nameOfApp:{
    color: '#10B0F5',
    fontSize: 20,
    textAlign:'center',
    padding:10


  }
});

export default styles;

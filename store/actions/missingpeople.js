//this page is responsible for adding the form the the database or update or delete them 
import Missingpeople from '../../models/missingpeople';

export const SET_MISSINGPEOPLE = 'SET_MISSINGPEOPLE';
import ENV from '../../env';
import * as firebase from "firebase";

export const fetchMissingpeople = () => {
    return async dispatch => {
        // any async code you want!
        const userId = firebase.auth().currentUser.uid;
        try {
            const response = await fetch(
                'https://help-6faf1.firebaseio.com/missingpeople.json'
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }
            const resData = await response.json();
            const missingpeople = [];
            //console.log(resData);

            for (const key in resData) {
                missingpeople.push(
                    new Missingpeople(
                        key,
                        resData[key].ownerId,
                        resData[key].FullName,
                        resData[key].description,
                        resData[key].AgeAtDisappearance,
                        resData[key].MissingSince,
                        resData[key].photowastakenOn,
                        resData[key].imageUrl,
                        resData[key].lat,
                        resData[key].lng,
                    )
                );
            }

            dispatch({
                type: SET_MISSINGPEOPLE,
                missingpeople: missingpeople,
            });
            //console.log(Requests)
        } catch (err) {
            // send to custom analytics server
            throw err;
        }
    };
};


import React, { Component } from 'react';
import { SafeAreaView, FlatList } from 'react-native';
import { shape, func } from 'prop-types';
import { ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import * as firebase from "firebase";


import { loadUsers } from '../../actions/chat/actions';

import EmptyState from '../../components/common/EmptyState';
import styles from './styles';
const getUsers = ({ users, mainUserId}) =>
users ? [...Object.values(users)].filter(({ id }) => id !== mainUserId) : [];
class Users extends Component {
  componentDidMount() {
    this.props.loadUsers();
  }
  keyExtractor = (item, index) => `user-${index}`;
  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      onPress={() => this.props.goToMessage({ id: item.id, name: item.name })}
      leftAvatar={{ source: { uri: item.avatar } }}
      bottomDivider
    />
  );

  render() {
    const idd = firebase.auth().currentUser.uid;

    const { users, user } = this.props;
    const userList = getUsers({ users, mainUserId: idd });

    return (
      <SafeAreaView style={styles.container}>
        <EmptyState list={userList} />
        <FlatList keyExtractor={this.keyExtractor} data={userList} renderItem={this.renderItem} />
      </SafeAreaView>
    );
  }
}

Users.defaultProps = {
  users: []
};

Users.propTypes = {
  loadUsers: func.isRequired,
  goToMessage: func.isRequired,
  users: shape({}),
  user: shape({}).isRequired
};

const mapStateToProps = ({ sessionReducer, chatReducer }) => ({
  user: sessionReducer.user,
  users: chatReducer.users
});

const mapDispatchToProps = {
  loadUsers
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users);


class UserInfo {
    constructor(id,ownerId,namee,email,dateofbirth,phonenumber,adress,imageUrl,avatar) {
      this.id = id;
      this.ownerId = ownerId;
      this.namee = namee;
      this.email = email;
      this.dateofbirth = dateofbirth;
      this.phonenumber = phonenumber;
      this.adress = adress;
      this.imageUrl = imageUrl;
      this.avatar = avatar;
    }
  }
  
  export default UserInfo;
  
/*
This is the page to edit the request details after it has been submitted
*/


import React, {useEffect, useCallback, useReducer, useState} from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Platform,
    Alert,
    KeyboardAvoidingView
} from 'react-native';
import {Button} from 'react-native-paper';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import {useSelector, useDispatch} from 'react-redux';
import HeaderButton from '../../components/UI/HeaderButton';
import * as productsActions from '../../store/actions/requests';
import Input from '../../components/UI/Input';
import ImgPicker from '../../components/ImagePicher';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';

const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        const updatedValidates = {
            ...state.inputValidates,
            [action.input]: action.isValid
        };
        let updatedFormIsValid = true;
        for (const key in updatedValidates) {
            updatedFormIsValid = updatedFormIsValid && updatedValidates[key];
        }
        return {
            formIsValid: updatedFormIsValid,
            inputValidates: updatedValidates,
            inputValues: updatedValues
        };
    }
    return state;
};

const NewEditProductScreen = props => {

    const prodId = props.navigation.getParam('productId');
    const editedProduct = useSelector(state =>
        state.Requests.userRequests.find(prod => prod.id === prodId)
    );
    const [selectedImage, setSelectedImage] = useState(prodId ? editedProduct.imageUrl : '');
    const dispatch = useDispatch();
    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            title: editedProduct.title,
            description: editedProduct.description,
            quantityNeeded: editedProduct.quantityNeeded,
        },
        inputValidates: {
            title: true,
            description: true,
            quantityNeeded: true,
        },
        formIsValid: true
    });

    const imageTakenHandler = imageUri => {
        setSelectedImage(imageUri);
    };


    const submitHandler = useCallback(() => {
      

        formState.inputValues.imageUrl = selectedImage;
        dispatch(
            productsActions.updateRequests(
                prodId,
                formState.inputValues.title,
                formState.inputValues.description,
                formState.inputValues.quantityNeeded,
                formState.inputValues.imageUrl,

            )
        );

        props.navigation.goBack();
    }, [dispatch, prodId, formState]);

    useEffect(() => {
        props.navigation.setParams({submit: submitHandler});
    }, [submitHandler]);

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            });
        },
        [dispatchFormState]
    );

    return (
        <KeyboardAvoidingView
            style={{flex: 1}}
            behavior="padding"
            keyboardVerticalOffset={100}
        >
            <ScrollView>
                <View style={styles.form}>
                    <Input
                        id="title"
                        label="Title"
                        errorText="Please enter a valid title!"
                        keyboardType="default"
                        autoCapitalize="sentences"
                        autoCorrect
                        returnKeyType="next"
                        initialValue={editedProduct.title}
                        onInputChange={inputChangeHandler}
                    />

                    <Input
                        id="description"
                        label="Description"
                        errorText="Please enter a valid description!"
                        keyboardType="default"
                        returnKeyType="next"
                        autoCapitalize="sentences"
                        autoCorrect
                        multiline
                        numberOfLines={3}
                        onInputChange={inputChangeHandler}
                        initialValue={editedProduct.description}
                    />

                    <Input
                        id="quantityNeeded"
                        label="Quantity"
                        errorText="Please enter the quantity needed!"
                        keyboardType="decimal-pad"
                        onInputChange={inputChangeHandler}
                        initialValue={editedProduct.quantityNeeded.toString()}
                    />

                    <ImgPicker onImageTaken={imageTakenHandler}/>

                    <Button onPress={submitHandler}>Update Product</Button>

                </View>
            </ScrollView>
        </KeyboardAvoidingView>

    );
};




NewEditProductScreen.navigationOptions = navData => {
    const submitFn = navData.navigation.getParam('submit');
    return {
        headerTitle: 'Edit Product',
        headerRight: () => {
            return <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Add"
                    iconName={Platform.OS === 'android' ? 'md-checkmark' : 'ios-checkmark'}
                    onPress={submitFn}
                />
            </HeaderButtons>;
        }


    };
};

const styles = StyleSheet.create({
    form: {
        margin: 20,

    }
});

export default NewEditProductScreen;


import {
    DELETE_ProgressUpdate,
    CREATE_ProgressUpdate,
    UPDATE_ProgressUpdate,
    SET_ProgressUpdate
  } from '../actions/ProgressUpdate';
  import ProgressUpdate from '../../models/ProgressUpdate';
  
  const initialState = {
    availableProgressUpdates: [],
    userProgressUpdates: []
  };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case SET_ProgressUpdate:
        return {
          availableProgressUpdates: action.ProgressUpdates,
          userProgressUpdates: action.userProgressUpdates
        };
      case CREATE_ProgressUpdate:
        const newProgressUpdate = new ProgressUpdate(
          action.ProgressUpdateData.id,
          action.ProgressUpdateData.ownerId,
          action.ProgressUpdateData.title,
          action.ProgressUpdateData.imageUrl,
          action.ProgressUpdateData.description,
          action.ProgressUpdateData.price,
          action.ProgressUpdateData.name
        );
        return {
          ...state,
          availableProgressUpdates: state.availableProgressUpdates.concat(newProgressUpdate),
          userProgressUpdates: state.userProgressUpdates.concat(newProgressUpdate)
        };
      case UPDATE_ProgressUpdate:
        const ProgressUpdateIndex = state.userProgressUpdate.findIndex(
          prod => prod.id === action.pid
        );
        const updatedProgressUpdate = new ProgressUpdate(
          action.pid,
          state.userProgressUpdates[ProgressUpdateIndex].ownerId,
          action.ProgressUpdateData.title,
          action.ProgressUpdateData.imageUrl,
          action.ProgressUpdateData.description,
          state.userProgressUpdates[ProgressUpdateIndex].price,
        );
        const updatedUserProgressUpdates = [...state.userProgressUpdates];
        updatedUserProgressUpdates[ProgressUpdateIndex] = updatedProgressUpdate;
        const availableProgressUpdateIndex = state.availableProgressUpdates.findIndex(
          prod => prod.id === action.pid
        );
        const updatedAvailableProgressUpdates = [...state.availableProgressUpdates];
        updatedAvailableProgressUpdates[availableProgressUpdateIndex] = updatedProgressUpdate;
        return {
          ...state,
          availableProgressUpdates: updatedAvailableProgressUpdates,
          userProgressUpdates: updatedUserProgressUpdates
        };
      case DELETE_ProgressUpdate:
        return {
          ...state,
          userProgressUpdates: state.userProgressUpdates.filter(
            ProgressUpdate => ProgressUpdate.id !== action.pid
          ),
          availableProgressUpdates: state.availableProgressUpdates.filter(
            ProgressUpdate => ProgressUpdate.id !== action.pid
          )
        };
    }
    return state;
  };
  
  
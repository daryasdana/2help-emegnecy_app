import moment from 'moment';

class Accepted {
  constructor(id,ownerId, items, totalAmount, date,Username,Useremail,ownerIdd) {
    this.id = id;
    this.ownerId=ownerId;
    this.items = items;
    this.totalAmount = totalAmount;
    this.date = date;
    this.Username=Username;
    this.Useremail=Useremail;
    this.ownerIdd=ownerIdd;
  }

  get readableDate() {
   
    return moment(this.date).format('MMMM Do YYYY, hh:mm');
  }
}

export default Accepted;

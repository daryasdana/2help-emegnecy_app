import {
    SET_MISSINGPEOPLE
  } from '../actions/missingpeople';

  const initialState = {
    availableMissingpeople: [],
  
  };
  export default (state = initialState, action) => {
    switch (action.type) {
      case SET_MISSINGPEOPLE:
        return {
          availableMissingpeople: action.missingpeople,
        };
        
      }
    return state;
  };
  
  
  
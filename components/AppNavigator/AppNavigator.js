/* eslint-disable global-require */
import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import SingInContainer from "../../components/Auth/SingIn";
import ProgressUpdate from "../../src/containers/ProgressUpdate";
import AddProgressUpdate from "../../src/containers/AddProgressUpdate";
import ProgressUpdateDetailScreen from "../../src/containers/ProgressUpdateDetailScreen";
import AcceptedRequests from "../../src/containers/AcceptedRequests";
import SignUpContainer from "../../components/Auth/SignUp";
import HomeContainer from "../../components/Home";
import Users from "../../components/Users";
import Messages from "../../components/Chat";
import { withNavigation, getNavigationOptions } from "./utils";
import AllMissingPeoples from "../../src/containers/AllMissingPeoples";
import AllRequests from "../../src/containers/AllRequests";
import UserRequestsScreen from "../../src/containers/UserRequestsScreen";
import DetailScreen from "../../src/containers/RequestDetailScreen";
import MissingPeopleDetailPage from "../../src/containers/MissingPeopleDetailPage";
import ProfileDetail from "../../src/containers/ProfileDetail";
import MapScreen from "../../src/containers/MapScreen";
import EditProfilePage from "../../src/containers/EditProfilePage";
import NewEditProductScreen from "../../src/containers/NewEditProductScreen";
import AddRequestScreen from "../../src/containers/AddRequestScreen";
import ProfilePage from "../../src/containers/ProfilePage";
import { Ionicons } from "@expo/vector-icons";
import Settings from "../../src/containers/Settings";

const TabNavigator = createBottomTabNavigator(
  {
    HomeTab: {
      screen: withNavigation(({ navigateTo, navigation }) => (
        <HomeContainer
          onSignOut={navigateTo("SignIn")}
          navigation={navigation}
        />
      )),
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: <Icon name="home" size={30} color="gray" />,
      },
    },
    UsersTab: {
      screen: withNavigation(({ navigateTo }) => (
        <Users
          goToMessage={({ id, name }) => navigateTo("Messages")({ id, name })}
        />
      )),
      navigationOptions: {
        tabBarLabel: "Chat",
        tabBarIcon: <Icon name="comments" size={30} color="gray" />,
      },
    },
    ProfilePage: {
      screen: ProfilePage,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="ios-person" size={30} color={tintColor} />
        ),
      },
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons name="ios-settings" size={24} color={tintColor} />
        ),
      },
    },
  },
  {
    initialRouteName: "HomeTab",
  }
);

const ModalStackNavigator = createStackNavigator(
  {
    Welcome2Help: {
      screen: TabNavigator,
    },
    Messages: {
      screen: Messages,
      navigationOptions: ({ navigation: { state } }) => {
        const { name: title } = state.params;
        return { ...getNavigationOptions({ title }) };
      },
    },

    UserRequestsScreen: UserRequestsScreen,
    NewEditProduct: NewEditProductScreen,
    AddRequests: AddRequestScreen,
    Map: MapScreen,
    AddProgressUpdate: AddProgressUpdate,
    ProgressUpdateDetailScreen,
    ProgressUpdateDetailScreen,
    NewRequests: AllRequests,
    ProgressUpdate: ProgressUpdate,
    AcceptedRequests: AcceptedRequests,
    ProductDetail: DetailScreen,
    MissingPeopleDetailPage: MissingPeopleDetailPage,
    AllMissingPeoples: AllMissingPeoples,
    Map: MapScreen,
    EditProfilePage: EditProfilePage,
    ProfileDetail: ProfileDetail,
  },
  {
    mode: "card",
  }
);

const SignedOutNavigator = createStackNavigator(
  {
    SignIn: {
      screen: withNavigation(({ navigateTo }) => (
        <SingInContainer
          onSignUp={navigateTo("SignUp")}
          onSignedIn={navigateTo("SignedIn")}
        />
      )),
    },
    SignUp: {
      screen: withNavigation(({ navigateTo }) => (
        <SignUpContainer onSignedIn={navigateTo("SignedIn")} />
      )),
    },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#FFFFFF",
      },
    },
    initialRouteName: "SignIn",
  }
);

const SignedInNavigator = (props) => (
  // <PushNotifications>
  <ModalStackNavigator {...props} />
  // </PushNotifications>
);
SignedInNavigator.router = ModalStackNavigator.router;

const MainNavigator = createSwitchNavigator({
  SignedOut: {
    screen: SignedOutNavigator,
  },
  SignedIn: {
    screen: SignedInNavigator,
  },
});

export default createAppContainer(MainNavigator);

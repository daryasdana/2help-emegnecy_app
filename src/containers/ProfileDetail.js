import React, { useEffect, useState, useCallback } from "react";

import {
  ScrollView,
  View,
  Text,
  Image,
  Button,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
  FlatList,
} from "react-native";
import Colors from "../../constants/Colors";
import * as firebase from "firebase";
import "firebase/firestore";
import { createReview } from "../../store/actions/review";
import { useSelector, useDispatch } from "react-redux";
import * as reviewActions from "../../store/actions/review";

const ProfileDetail = (props) => {
  const [error, setError] = useState();
  const userInfoId = props.navigation.getParam("userInfoId");
  const [feedback, setFeedBack] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const selectedProduct = useSelector((state) =>
    state.userInfo.availableUserInfo.find((prod) => prod.id === userInfoId)
  );
  const dispatch = useDispatch();
  const reviews = useSelector((state) => state.review.userReviews);

 // console.log("actualrevie", reviews);

  const loadReviews = useCallback(
    async (uerInfoId) => {
      setError(null);
      setIsLoading(true);
      try {
        await dispatch(reviewActions.fetchReview(uerInfoId));
      } catch (err) {
        setError(err.message);
      }
      setIsLoading(false);
    },
    [dispatch, setIsLoading, setError]
  );
  useEffect(() => {
    loadReviews(userInfoId);
  }, [dispatch, loadReviews]);

  const onPressSubmit = async (review) => {
    const db = firebase.firestore();

    db.collection("reviews").add({
      review,
    
    });
    setFeedBack("");
    setIsLoading(true);
    await dispatch(reviewActions.createReview(review, userInfoId));
    setIsLoading(false);
    loadReviews(userInfoId);

    alert("Submitted review successfully");
  };

  if (isLoading) {
    return (
      
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.centered}>
        <Text>An error occurred!</Text>
        <Button
          title="Try again"
          onPress={loadReviews}
          color={Colors.primary}
        />
      </View>
    );
  }

  return (
    <KeyboardAvoidingView
    behavior={Platform.OS === "ios" ? "padding" : null}
    style={{ flex: 1 }}
>
    <ScrollView>
      
      <Image style={styles.image} source={{ uri: selectedProduct.imageUrl }} />
      <View style={styles.actions}></View>
      <Text style={styles.description}>Name: {selectedProduct.namee}</Text>
      <Text style={styles.description}>{selectedProduct.dateofbirth}</Text>
      <Text style={styles.description}>{selectedProduct.phonenumber}</Text>
      <Text style={styles.price}>{selectedProduct.adress}</Text>
      
      <TextInput
        style={styles.textInput}
        placeholder="FeedBack"
        autoCapitalize="none"
        onChangeText={(feedback) => {
          setFeedBack(feedback);
        }}
        value={feedback}
        underlineColorAndroid="transparent"
        numberOfLines={10}
      />

      <Button
        color={Colors.primary}
        title={"Submit Feedback"}
        onPress={() => {
          onPressSubmit(feedback);
        }}
      />
      <Text
        style={{
          textAlign: "center",
          fontSize: 18,
          margin: 12,
          backgroundColor: "#F06292",
          height: 30,
        }}
      >
        {"Reviews"}
      </Text>
      {!isLoading && reviews.length === 0 ? (
        <View style={styles.centered}>
          <Text style={{ textAlign: "center" }}>No reviews were added </Text>
        </View>
      ) : (
        <FlatList
          data={reviews}
          keyExtractor={(item) => item.id}
          renderItem={(itemData) => (
            <Text
              style={{
                textAlign: "center",
                fontSize: 15,
              }}
            >
             by {itemData.item.name } :  {itemData.item.review } 
            </Text>
          )}
        />
      )}
      
    </ScrollView>
   
    </KeyboardAvoidingView>
  );
};

ProfileDetail.navigationOptions = (navData) => {
  return {
    headerTitle: navData.navigation.getParam("userInfoName"),
  };
};

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 300,
  },
  textInput: {
    backgroundColor: "#ffffff",
    padding: 10,
    height: 50,
    margin: 10,
    borderRadius: 5,
  },
  actions: {
    marginVertical: 10,
    alignItems: "center",
  },
  price: {
    fontSize: 20,
    color: "#888",
    textAlign: "center",
    marginVertical: 20,
  },
  description: {
    fontSize: 14,
    textAlign: "center",
    marginHorizontal: 20,
  },
  quantity: {
    fontSize: 20,
    color: "#888",
    textAlign: "center",
    marginVertical: 20,
  },
  mapPreview: {
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: "white",
    height: 300,
    margin: 5,
  },
});

export default ProfileDetail;


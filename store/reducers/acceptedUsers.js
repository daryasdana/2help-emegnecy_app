import { ADD_ORDER, SET_ORDERS } from '../actions/acceptedUsers';
import Accepted from '../../models/Accepted';

const initialState = {
  acceptedVW: []
};
export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ORDERS:
      return {
        acceptedVW: action.acceptedVW
      };
    case ADD_ORDER:
      const newaccepted = new Accepted(
        action.acceptedUsersData.id,
        action.acceptedUsersData.ownerId,
        action.acceptedUsersData.items,
        action.acceptedUsersData.amount,
        action.acceptedUsersData.date,
        
      );
      return {
        ...state,
        acceptedVW: state.acceptedVW.concat(newaccepted)
      };
  }

  return state;
};


import Accepted from '../../models/Accepted';
export const SET_USERINFO = 'SET_USERINFO';
export const ADD_ORDER = 'ADD_ORDER';
export const SET_ORDERS = 'SET_ORDERS';
  import * as firebase from "firebase";

export const fetchOrders = () => {
const userId=firebase.auth().currentUser.uid;

  return async dispatch => {
    try {
      const response = await fetch(
        `https://help-6faf1.firebaseio.com/AcceptedRequests.json`
      );

      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      const loadedOrders = [];
      const acceptedVW = [];
      //console.log(resData)

      for (const key in resData) {
        loadedOrders.push(
          new Accepted(
            key,
            resData[key].id,
            resData[key].cartItems,
            resData[key].totalAmount,
            new Date(resData[key].date),
            resData[key].Username,
            resData[key].Useremail,
            resData[key].ownerId,
            
          )
        );
      }
      dispatch({ type: SET_ORDERS,
         acceptedVW: loadedOrders
         //.filter(prod => prod.ownerId === userId)


        });
    } catch (err) {
      throw err;
    }
  };

};



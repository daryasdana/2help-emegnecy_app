
import {
  DELETE_REQUEST,
  CREATE_REQUEST,
  UPDATE_REQUEST,
  SET_REQUEST
} from '../actions/requests';
import Product from '../../models/product';

const initialState = {
  availableRequest: [],
  userRequests: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_REQUEST:
      return {
        availableRequest: action.Requests,
        userRequests: action.userRequests
      };
    case CREATE_REQUEST:
      const newRequest = new Product(
        action.requestData.id,
        action.requestData.ownerId,
        action.requestData.title,
        action.requestData.imageUrl,
        action.requestData.description,
        action.requestData.price,
        action.requestData.image,
        action.requestData.quantityNeeded,
        action.requestData.image,
        //action.requestData.address,
        action.requestData.lat,
        action.requestData.lng,
        
      );
      return {
        ...state,
        availableRequest: state.availableRequest.concat(newRequest),
        userRequests: state.userRequests.concat(newRequest)
      };
    case UPDATE_REQUEST:
      const productIndex = state.userRequests.findIndex(
        prod => prod.id === action.pid
      );
      const updatedRequest = new Product(
        action.pid,
        state.userRequests[productIndex].ownerId,
        action.requestData.title,
        action.requestData.imageUrl,
        action.requestData.description,
        state.userRequests[productIndex].price,
        action.requestData.quantityNeeded
      );
      const updatedUserRequest = [...state.userRequests];
      updatedUserRequest[productIndex] = updatedRequest;
      const availableRequestIndex = state.availableRequest.findIndex(
        prod => prod.id === action.pid
      );
      const updatedAvailableRequest = [...state.availableRequest];
      updatedAvailableRequest[availableRequestIndex] = updatedRequest;
      return {
        ...state,
        availableRequest: updatedAvailableRequest,
        userRequests: updatedUserRequest
      };
    case DELETE_REQUEST:
      return {
        ...state,
        userRequests: state.userRequests.filter(
          product => product.id !== action.pid
        ),
        availableRequest: state.availableRequest.filter(
          product => product.id !== action.pid
        )
      };
  }
  return state;
};


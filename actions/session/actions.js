import firebaseService from "../../enviroments/firebase";
import * as types from "./actionsTypes";

const FIREBASE_REF_USERS = firebaseService.database().ref("Users");
const avatar = `https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png`;

const createUser = ({
  user,
  name,
  phonenumber,
  adress,
  dateofbirth,
  roll,
  email,
  dispatch,
}) => {
  const id = user.uid;
  return FIREBASE_REF_USERS.child(id)
    .set({
      id,
      name,
      phonenumber,
      adress,
      dateofbirth,
      roll: "Users",
      email,
      avatar,
    })
    .then(() => {
      updateUser({
        user,
        name,
        phonenumber,
        adress,
        dateofbirth,
        roll,
        avatar,
        dispatch,
      });
    });
};

const updateUser = ({ user, name, avatar, dispatch }) =>
  user
    .updateProfile({
      displayName: name,
      photoURL: avatar,
    })
    .then(() => {
      dispatch(signupSuccess(user));
    });

export const restoreSession = () => (dispatch) => {
  dispatch(sessionLoading());
  firebaseService.auth().onAuthStateChanged((user) => {
    if (user) {
      dispatch(sessionSuccess(user));
    } else {
      dispatch(sessionLogout());
    }
  });
};

export const loginUser = ({ email, password }) => (dispatch) => {
  dispatch(sessionLoading());
  firebaseService
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((user) => {
      dispatch(sessionSuccess(user));
    })
    .catch((error) => {
      dispatch(sessionError(error.message));
    });
};

export const signupUser = ({
  name,
  phonenumber,
  adress,
  dateofbirth,
  email,
  password,
}) => (dispatch) => {
  dispatch(sessionLoading());
  firebaseService
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then((authData) => {
      createUser({
        user: authData.user,
        name,
        phonenumber,
        adress,
        dateofbirth,
        email,
        dispatch,
      });
    })
    .catch((error) => {
      dispatch(sessionError(error.message));
    });
};

export const logoutUser = () => (dispatch) => {
  dispatch(sessionLoading());
  firebaseService
    .auth()
    .signOut()
    .then(() => {
      dispatch(sessionLogout());
    })
    .catch((error) => {
      dispatch(sessionError(error.message));
    });
};

const sessionLoading = () => ({
  type: types.SESSION_LOADING,
});

const sessionSuccess = (user) => ({
  type: types.SESSION_SUCCESS,
  user,
});

const signupSuccess = (user) => ({
  type: types.SIGNUP_SUCCESS,
  user,
});

export const sessionError = (error) => ({
  type: types.SESSION_ERROR,
  error,
});

const sessionLogout = () => ({
  type: types.SESSION_LOGOUT,
});

import React, { Component,useState } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import MapPreviw from '../../components/MapPreview';


const MissingPeopleDetailPage = props => {
const missingpeopleId = props.navigation.getParam('missingpeopleId');
const selectedMissingpeople = useSelector(state =>
    state.missingpeople.availableMissingpeople.find(prod => prod.id === missingpeopleId)
   
  );
    
/* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/

  /*
this function will call when the user press the map. 
It will navigate to the map page
  */

  const selectedLocation = {lat:selectedMissingpeople.lat, lng:selectedMissingpeople.lng};
  const showMapHandler = () => {
    props.navigation.navigate('Map', {
      readonly: true,
      initialLocation: selectedLocation
    });
  };


  return (
    
    <ScrollView>
      <Image style={styles.image} source={{ uri: selectedMissingpeople.imageUrl }} />
      <View style={styles.actions}>
        
      </View>
      <Text style={styles.price}>Age At Missing:{selectedMissingpeople.AgeAtDisappearance}</Text>
      <Text style={styles.description}>{selectedMissingpeople.description}</Text>
      <Text style={styles.price}>Missing Since:{selectedMissingpeople.MissingSince}</Text>
      <Text style={styles.price}>Photo was taken on :{selectedMissingpeople.photowastakenOn}</Text>
      <Text>Location of Last Seen</Text>
      <MapPreviw
      style={styles.mapPreview}
      
      location={selectedLocation}
          onPress={showMapHandler}
          />

      
    </ScrollView>

  );
};

MissingPeopleDetailPage.navigationOptions = navData => {
  return {
    headerTitle: navData.navigation.getParam('missingpeopleFullName')
  };
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 300
  },
  actions: {
    marginVertical: 10,
    alignItems: 'center'
  },
  price: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20
  },
  description: {
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: 20
  },
  quantity: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20
  },
  mapPreview: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 300,
    margin: 5
  }
});


export default MissingPeopleDetailPage;

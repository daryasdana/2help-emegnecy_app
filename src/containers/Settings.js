
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    ScrollView,

} from 'react-native';
import * as firebase from "firebase";

export default class Settings extends Component {

        state = {
            email: "",
            displayName: ""
        };
        componentDidMount() {
            const { email, displayName } = firebase.auth().currentUser;
    
            this.setState({ email, displayName });
        }

    render() {
        const { navigate } = this.props.navigation;
       const signOutUser = () => {
            firebase.auth().signOut();
            navigate('SignIn',)
        };
        return (
            <ScrollView style={{padding: 20}}>
                <Text 
                    style={styles.greeting}>
                    Settings Page
                </Text>
                <TouchableOpacity style={styles.btnTxt} onPress={signOutUser}>
                    <Text>Logout</Text>
                </TouchableOpacity>
                </ScrollView>
                )
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },

    btnTxt:{
        
        backgroundColor: "#FFD700",
        padding:15,
        width: "45%",
        },
        image:{
             margin:20,
            
            marginBottom:50,
            width: 70,
            height: 70,
            resizeMode: 'contain',
           
           },
           greeting: {
            fontSize:20,
                 textAlign: 'center',
                 margin:100,
                 color: "#FF6E40"
          },

});
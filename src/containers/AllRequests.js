/*
this is the page where I am showing all the request
*/

import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import RequestItem from '../../components/shop/RequestItem';
import * as productsActions from '../../store/actions/requests';
import Colors from '../../constants/Colors';

/* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/ 
const AllRequests = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const Requests = useSelector(state => state.Requests.availableRequest);
    const dispatch = useDispatch();

    const loadProducts = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(productsActions.fetchRequest());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);
  /*
  this willFocus is for refreshing the page without leaving the page 
  */

    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadProducts
        );
        return () => {
            willFocusSub.remove();
        };
    }, [loadProducts]);

    useEffect(() => {
        loadProducts();
    }, [dispatch, loadProducts]);
/*
the selectItemHandler function will call when the user 
touch the request and it will take them to that request detail page 
we are passing the id and the title of the request so it know which reqest to show 
*/

    const selectItemHandler = (id, title) => {
        props.navigation.navigate('ProductDetail', {
            productId: id,
            productTitle: title
        });
    };
     /*
      if the data is not loading is will show them this error message 
      with with a button where they can click and it will reload the data
      */
    if (error) {
        return (
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadProducts}
                    color={Colors.primary}
                />
            </View>
        );
    }
  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */
    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }
//If No Request is submitted it will show this message 
    if (!isLoading && Requests.length === 0) {
        return (
            <View style={styles.centered}>
                <Text>No requests found!</Text>
            </View>
        );
    }

/*
    this is for rendering the data and extracting them by their unique id
    I am using a FlatList because I don't know how long the list might be
*/
    return (
        <FlatList
            style={styles.p}
            data={Requests}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <RequestItem
                    image={itemData.item.imageUrl}
                    title={itemData.item.title}
                    price={itemData.item.price}
                    quantityNeeded={itemData.item.quantityNeeded}
                    address={itemData.item.address}
                    onSelect={() => {
                        selectItemHandler(itemData.item.id, itemData.item.title);
                    }}
                >
                    <Button
                        // color={Colors.primary}
                        style={styles.btn}
                        title="View Details"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.title);
                        }}
                    />

                </RequestItem>
            )}
        />
    );
};

AllRequests.navigationOptions = navData => {
    return {
        headerTitle: 'All Requests',

    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},

    pr: {
        padding: 140,
        margin: 100
    },
    btn: {
        marginTop: 100,
        padding: 10,
    }
});

export default AllRequests;

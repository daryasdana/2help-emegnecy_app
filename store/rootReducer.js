import { combineReducers } from "redux";
import navReducer from "../reducers/navigation";
import sessionReducer from "../reducers/session";
import chatReducer from "../reducers/chat";
import productsReducer from "../store/reducers/requests";
import userInfoReducer from "../store/reducers/userInfo";
import missingpeopleReducer from "../store/reducers/missingpeople";
import ordersReducer from "./reducers/acceptedUsers";
import ProgressUpdateReducer from "../store/reducers/ProgressUpdate";
import reviewReducer from "../store/reducers/review";

export default combineReducers({
  nav: navReducer,
  sessionReducer,
  chatReducer,
  Requests: productsReducer,
  missingpeople: missingpeopleReducer,
  acceptedVW: ordersReducer,
  ProgressUpdate: ProgressUpdateReducer,
  review: reviewReducer,
  userInfo: userInfoReducer,
});


import React, { useState, useEffect, useCallback } from "react";
import { View, Text, Button, StyleSheet, FlatList } from "react-native";

import Colors from "../../constants/Colors";
import Card from "../UI/Card";
import Profile from "../shop/Profile";
import { useSelector, useDispatch } from "react-redux";
import * as userInfoActions from "../../store/actions/userInfo";


const AcceptedItem = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [showDetails, setShowDetails] = useState(false);
  const dispatch = useDispatch();
  const acceptedVW = useSelector((state) => state.acceptedVW.acceptedVW);
  const userUserInfo = useSelector((state) => state.userInfo.userUserInfoo);
  const userIds = [];
  for (const { ownerIdd, items } of acceptedVW) {
    for (const item of items) {
      if (props.productId === item.productId) {
        for (const user of userUserInfo) {
          if (user.id === ownerIdd) {
            userIds.push(user);
          }
        }
      }
    }
  }

  const selectItemHandler = (id, namee) => {
    props.navigation.navigate("ProfileDetail", {
      userInfoId: id,
      userInfoName: namee,
    });
  };

  const loadProducts = useCallback(async () => {
    setError(null);
    setIsLoading(true);
    try {
      await dispatch(userInfoActions.fetchUserInfo());
    } catch (err) {
      setError(err.message);
    }
    setIsLoading(false);
  }, [dispatch, setIsLoading, setError]);
  useEffect(() => {
    loadProducts();
  }, [dispatch, loadProducts]);

  return (
    <Card style={styles.AcceptedItem}>
      <View style={styles.summary}>
        <Text style={styles.totalAmount}>£{props.price}</Text>
        <Text style={styles.totalAmount}>{props.title}</Text>
      </View>
      <Button
        color={Colors.primary}
        title={showDetails ? "Hide Details" : "Show Details"}
        onPress={() => {
          setShowDetails((prevState) => !prevState);
        }}
      />
      {showDetails && (
        <View style={styles.detailItems}>
          {/* this is where I am showing the profile in the Accepted request page */}
          <FlatList
            data={userIds}
            keyExtractor={(item) => item.id}
            renderItem={(itemData) => (
              <Profile
                image={itemData.item.imageUrl}
                title={itemData.item.namee}
                price={itemData.item.email}
                phonenumber={itemData.item.phonenumber}
                adress={itemData.item.adress}
                quantityNeeded={itemData.item.dateofbirth}
                onSelect={() => {
                  selectItemHandler(itemData.item.id, itemData.item.namee);
                }}
              >
                <Button
                  color={Colors.primary}
                  style={styles.btn}
                  title="Give feedback to volunteer"
                  onPress={() => {
                    selectItemHandler(itemData.item.id, itemData.item.namee);
                  }}
                />
              </Profile>
            )}
          />
        </View>
      )}
    </Card>
  );
};

AcceptedItem.navigationOptions = (navData) => {
  return {
    headerTitle: navData.navigation.getParam("userInfoName"),
  };
};

const styles = StyleSheet.create({
  AcceptedItem: {
    margin: 20,
    padding: 10,
    alignItems: "center",
  },
  summary: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    marginBottom: 15,
  },
  totalAmount: {
    fontSize: 16,
  },
  date: {
    fontSize: 16,

    color: "#888",
  },
  detailItems: {
    width: "100%",
  },
  name: {},
});

export default AcceptedItem;


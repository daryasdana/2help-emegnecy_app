class Review {
  constructor(id, ownerId, review,name) {
    this.id = id;
    this.ownerId = ownerId;
    this.review = review;
    this.name=name;
  }
}

export default Review;

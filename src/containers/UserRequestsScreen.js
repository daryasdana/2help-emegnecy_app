/*
This is the page where the emergency team can see what request they
submitted and they can delete it or they can change the information
*/


import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    ActivityIndicator,
    StyleSheet,
    Alert
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import RequestItem from '../../components/shop/RequestItem';
import * as productsActions from '../../store/actions/requests';
import Colors from '../../constants/Colors';

const UserRequestsScreen = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const dispatch = useDispatch();
    const userRequests = useSelector(state => state.Requests.userRequests);
/* 
the useSelector will allow us to access the reduct store 
user useSelector will takes a function and access the state and redunt what we need 
*/

    const loadProducts = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(productsActions.fetchRequest());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);
  /*
  this willFocus is for refreshing the page without leaving the page 
  */
    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadProducts
        );

        return () => {
            willFocusSub.remove();
        };
    }, [loadProducts]);

    useEffect(() => {
        loadProducts();
    }, [dispatch, loadProducts]);

 /*
the selectItemHandler function will call when the user 
touch the request and it will take them to that request detail page 
we are passing the id and the title of the request so it know which reqest to show 
*/   

    const selectItemHandler = (id, title) => {
        props.navigation.navigate('ProductDetail', {
            productId: id,
            productTitle: title
        });
    };

    const updateItemHandler = (id) => {
        props.navigation.navigate('NewEditProduct', {
            productId: id,
        });
    };

    const deleteHandler = (id) => {
        Alert.alert('Are you sure?', 'Do you  want to delete this request?', [
          { text: 'No', style: 'default' },
          {
            text: 'Yes',
            style: 'destructive',
            onPress: () => {
              dispatch(productsActions.deleteRequest(id));
            }
          }
        ]);
      };
     /*
      if the data is not loading is will show them this error message 
      with with a button where they can click and it will reload the data
      */
    if (error) {
        return (
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadProducts}
                    color={Colors.primary}
                />
            </View>
        );
    }
  /*
  this is saying if it is Loading it will show the ActivityIndicator
  until it will load all the data. 
  this will usually show if the internet connection is slow
  */
    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }
//If No Request is submitted it will show this message 

    if (!isLoading && userRequests.length ===0) {
        return (
            <View style={styles.centered}>
                <Text>You did not send any requests!</Text>
            </View>
        );
    }
/*
    this is for rendering the data and extracting them by their unique id
    I am using a FlatList because I don't know how long the list might be
*/
    return (
        <FlatList
            style={styles.p}
            data={userRequests}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <RequestItem
                    image={itemData.item.imageUrl}
                    title={itemData.item.title}
                    price={itemData.item.price}
                    quantityNeeded={itemData.item.quantityNeeded}
                    onSelect={() => {
                        selectItemHandler(itemData.item.id, itemData.item.title);
                    }}
                >
                    <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title="View Details"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.title);
                        }}
                    />

                    <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title="Edit"
                        onPress={() => {
                            updateItemHandler(itemData.item.id);
                        }}
                    />
                     <Button
                         color={Colors.primary}
                         title="Delete"
                         onPress={deleteHandler.bind(this, itemData.item.id)}
                     />

                </RequestItem>
            )}
        />
    );
};

UserRequestsScreen.navigationOptions = navData => {
    return {
        headerTitle: 'My Requests',

    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},

    pr: {
        padding: 140,
        margin: 100
    },
    btn: {
        marginTop: 100,
        padding: 10,
    }
});

export default UserRequestsScreen;

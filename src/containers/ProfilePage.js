
import React, {useState, useEffect, useCallback} from 'react';
import {
    View,
    Text,
    FlatList,
    Button,
    Platform,
    ActivityIndicator,
    StyleSheet,
    Alert
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Profile from '../../components/shop/Profile';
import * as userInfoActions from '../../store/actions/userInfo';
import Colors from '../../constants/Colors';

const ProfilePage = props => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState();
    const userInfo = useSelector(state => state.userInfo.availableUserInfo);
    const dispatch = useDispatch();
    const userUserInfo = useSelector(state => state.userInfo.userUserInfo);

    const loadProducts = useCallback(async () => {
        setError(null);
        setIsLoading(true);
        try {
            await dispatch(userInfoActions.fetchUserInfo());
        } catch (err) {
            setError(err.message);
        }
        setIsLoading(false);
    }, [dispatch, setIsLoading, setError]);

    useEffect(() => {
        const willFocusSub = props.navigation.addListener(
            'willFocus',
            loadProducts
        );

        return () => {
            willFocusSub.remove();
        };
    }, [loadProducts]);

    useEffect(() => {
        loadProducts();
    }, [dispatch, loadProducts]);

    const selectItemHandler = (id, namee) => {
        props.navigation.navigate('ProfileDetail', {
            userInfoId: id,
            userInfoName: namee
        });
    };

    const updateItemHandler = (id) => {
        props.navigation.navigate('EditProfilePage', {
            userInfoId: id,
        });
    };

 

    if (error) {
        return (
            <View style={styles.centered}>
                <Text>An error occurred!</Text>
                <Button
                    title="Try again"
                    onPress={loadProducts}
                    color={Colors.primary}
                />
            </View>
        );
    }

    if (isLoading) {
        return (
            <View style={styles.centered}>
                <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
        );
    }

    
    return (
        <FlatList
            style={styles.p}
            data={userUserInfo}
            
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <Profile

                    image={itemData.item.imageUrl}
                    title={itemData.item.namee}
                    price={itemData.item.email}
                    phonenumber={itemData.item.phonenumber}
                    adress={itemData.item.adress}
                    quantityNeeded={itemData.item.dateofbirth}
                    // onSelect={() => {
                    //     selectItemHandler(itemData.item.id, itemData.item.namee);
                    // }}
                    onSelect={() => {
                        updateItemHandler(itemData.item.id);
                    }}
                >

                        <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title="Edit"
                        onPress={() => {
                            updateItemHandler(itemData.item.id);
                        }}
                            />
                    {/* <Button
                         color={Colors.primary}
                        style={styles.btn}
                        title=">"
                        onPress={() => {
                            selectItemHandler(itemData.item.id, itemData.item.namee);
                        }}
                    /> */}

                    
                    

                </Profile>
            )}
        />
    );
};

ProfilePage.navigationOptions = navData => {
    return {
        headerTitle: 'My Profile',

    };
};

const styles = StyleSheet.create({
    centered: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    p: {
         //padding: 1,

    },
    pr: {
        padding: 140,
        margin: 100
    },
    btn: {
        marginTop: 100,
        padding: 10,
    }
});

export default ProfilePage;
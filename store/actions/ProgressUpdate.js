export const DELETE_ProgressUpdate = 'DELETE_ProgressUpdate';
export const CREATE_ProgressUpdate = 'CREATE_ProgressUpdate';
export const UPDATE_ProgressUpdate = 'UPDATE_ProgressUpdate';
export const SET_ProgressUpdate = 'SET_ProgressUpdate';
import ENV from '../../env';
import * as firebase from "firebase";
import ProgressUpdate from '../../models/ProgressUpdate';

export const fetchProgressUpdate = () => {
    return async dispatch => {
        // any async code you want!
        const userId = firebase.auth().currentUser.uid;
        const name = firebase.auth().currentUser.displayName
        try {
            const response = await fetch(
                'https://help-6faf1.firebaseio.com/ProgressUpdate.json'
            );

            if (!response.ok) {
                throw new Error('Something went wrong!');
            }

            const resData = await response.json();
            const ProgressUpdates = [];

            for (const key in resData) {
                ProgressUpdates.push(
                    new ProgressUpdate(
                        key,
                        resData[key].ownerId,
                        resData[key].title,
                        resData[key].description,
                        resData[key].price,
                        resData[key].imageUrl,
                        resData[key].name,
                        
                    )
                );
            }

            dispatch({
                type: SET_ProgressUpdate,
                ProgressUpdates: ProgressUpdates,
                userProgressUpdates:ProgressUpdates.filter(prod => prod.ownerId === userId)
                
            });
            // console.log(ProgressUpdates)
            // console.log('response')
    
        } catch (err) {
            throw err;
        }
    };
};

export const deleteProgressUpdate = ProgressUpdateId => {
    return async dispatch => {
        await fetch(
            `https://help-6faf1.firebaseio.com/ProgressUpdate/${ProgressUpdateId}.json`,
            {
                method: 'DELETE',
                
                
            }
        );
        dispatch({type: DELETE_ProgressUpdate, pid: ProgressUpdateId});
    };
};

const getImageFromFirebase = async (uri) => {
    const imageName = Math.round(+new Date()/1000) + "." + uri.substr(uri.lastIndexOf('.') +1);
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = firebase.storage().ref().child("ProgressUpdate/" + imageName);
    const snapshot = await ref.put(blob);
    const imgURL = await snapshot.ref.getDownloadURL();
    return imgURL
};

export const addProgressUpdate = (title, description, price, imageUri) => {
    return async dispatch => {
        const userId = firebase.auth().currentUser.uid;
        const name = firebase.auth().currentUser.displayName
        console.log(name)
        const imageUrl = await getImageFromFirebase(imageUri);
        const payload = JSON.stringify({
            title,
            description,
            price,
            ownerId: userId,
            imageUrl,
            name
        });
        
        const response = await fetch(
            'https://help-6faf1.firebaseio.com/ProgressUpdate.json',
            
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: payload,
               

            }
        );

        const resData = await response.json();
        
        dispatch({
            type: CREATE_ProgressUpdate,
            ProgressUpdateData: {
                id: resData.name,
                title,
                description,
                price,
                ownerId: userId,
                imageUrl,
                name,
               

            }
        });
        
    };
   
};

export const updateProgressUpdate = (id, title, description,quantityNeeded, imageUri) => {
    return async dispatch => {

        const imageUrl = await getImageFromFirebase(imageUri);
        await fetch(
            `https://help-6faf1.firebaseio.com/ProgressUpdate/${id}.json`,
            {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title,
                    description,
                    quantityNeeded,
                    imageUrl,
                })
            }
        );

        dispatch({
            type: UPDATE_ProgressUpdate,
            pid: id,
            ProgressUpdateData: {
                title,
                description,
                quantityNeeded,
                imageUrl,
            }
        });
    };
};

import { 
  ADD_USERINFO,
   SET_USERINFO,
   UPDATE_USERINFO,
   } from '../actions/userInfo';
import UserInfo from '../../models/userInfo';

const initialState = {
  availableUserInfo: [],
  userUserInfo: [],
  userUserInfoo:[]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USERINFO:
      return {
        //userUserInfo testt
        availableUserInfo:action.userInfo,
        userUserInfo: action.userUserInfo,
        userUserInfoo: action.userInfo
        //userInfo.filter
      };
    case ADD_USERINFO:
      const newUserInfo = new UserInfo(
        action.userInfoData.id,
        action.userInfoData.ownerId,
        action.userInfoData.namee,
        action.userInfoData.email,
        action.userInfoData.dateofbirth,
        action.userInfoData.phonenumber,
        action.userInfoData.adress,
        action.userInfoData.avatar,
        
      );
      return {
        ...state,
        availableUserInfo: state.availableUserInfo.concat(newUserInfo),
        userUserInfo: state.userUserInfo.concat(newUserInfo),
        userUserInfoo: state.userUserInfoo.concat(newUserInfo)
      };


      case UPDATE_USERINFO:
      const userInfoIndex = state.userUserInfo.findIndex(
        prod => prod.id === action.pid
      );

      const updatedUserInfo = new UserInfo(
        action.pid,
        state.userUserInfo[userInfoIndex].ownerId,
        action.userInfoData.name,
        action.userInfoData.email,
        action.userInfoData.dateofbirth,
        action.userInfoData.phonenumber,
        action.userInfoData.adress,
        action.userInfoData.avatar,

      );
    
      const updatedUserUserInfo = [...state.userUserInfo];
      updatedUserUserInfo[userInfoIndex] = updatedUserInfo;
      const availableUserInfoIndex = state.availableUserInfo.findIndex(
        prod => prod.id === action.pid
      );

      const updatedAvailableUserInfo = [...state.availableUserInfo];
      updatedAvailableUserInfo[availableUserInfoIndex] = updatedUserInfo;
      return {
        ...state,
        availableUserInfo: updatedAvailableUserInfo,
        userUserInfo: updatedUserUserInfo
      };
  }

  return state;
};
